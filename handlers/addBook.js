const middy = require('middy');
const {urlEncodeBodyParser, httpErrorHandler,jsonBodyParser} = require('middy/middlewares');
const {addResolver} = require('../requestResolvers/addBook');

const addHandler = middy(addResolver)
  .use(urlEncodeBodyParser({extended: true}))
  .use(jsonBodyParser())
  .use(httpErrorHandler());

module.exports = {
  add: addHandler
};
