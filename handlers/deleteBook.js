const middy = require('middy');
const {urlEncodeBodyParser, httpErrorHandler,jsonBodyParser} = require('middy/middlewares');
const {deleteResolver} = require('../requestResolvers/deleteBook');

const deleteHandler = middy(deleteResolver)
  .use(urlEncodeBodyParser({extended: true}))
  .use(jsonBodyParser())
  .use(httpErrorHandler());

module.exports = {
  delete : deleteHandler
};
