const middy = require('middy');
const {urlEncodeBodyParser, httpErrorHandler,jsonBodyParser} = require('middy/middlewares');
const {getAll} = require('../requestResolvers/getAllBooks');

const getAllHandler = middy(getAll)
  .use(urlEncodeBodyParser({extended: true}))
  .use(jsonBodyParser())
  .use(httpErrorHandler());

module.exports = {
  getAll : getAllHandler
};
