const middy = require('middy');
const {urlEncodeBodyParser, httpErrorHandler,jsonBodyParser} = require('middy/middlewares');
const {get} = require('../requestResolvers/getBookDetails');

const getHandler = middy(get)
  .use(urlEncodeBodyParser({extended: true}))
  .use(jsonBodyParser())
  .use(httpErrorHandler());

module.exports = {
  get : getHandler
};
