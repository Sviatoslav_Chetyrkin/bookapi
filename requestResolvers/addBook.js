const {addBook} = require('../services/addBook');

const addResolver = async (event) => {
  const data = JSON.parse(event.body);

  if (typeof data.name !== 'string') {
    console.error('Validation Failed');
    callback(null, {
      statusCode: 400,
      headers: { 'Content-Type': 'text/plain' },
      body: 'Couldn\'t create the book record.',
    });
    return;
  }

  const bookEntity = {
    name: data.name,
    releaseDate: data.releaseDate,
    authorName: data.authorName
  };

  try {
    const result = await addBook(bookEntity);
    console.log(result);
    return {
      statusCode: 200,
      body: JSON.stringify(result)
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error.statusCode || 501,
      headers: { 'Content-Type': 'text/plain' },
      body: 'Couldn\'t create the book record.',
    }
  }

};

module.exports = {
  addResolver
};
