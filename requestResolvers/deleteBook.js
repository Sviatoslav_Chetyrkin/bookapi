const {get} = require('lodash');
const {deleteBook} = require('../services/deleteBook');

const deleteResolver = async (event) => {

  const bookId = get(event, 'pathParameters.bookId', '');

  try {
    const result = await deleteBook(bookId);

    return {
      statusCode: 200,
      body: JSON.stringify({deleted: true})
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error.statusCode || 501,
      headers: { 'Content-Type': 'text/plain' },
      body: `Couldn\'t delete book record ${bookId}`,
    }
  }
};


module.exports = {
  deleteResolver
};
