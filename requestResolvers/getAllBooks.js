const middy = require('middy');
const {urlEncodeBodyParser, httpErrorHandler,jsonBodyParser} = require('middy/middlewares');
const {getAllBooks} = require('../services/getAllBooks');

const getAllResolver = async () => {
  try {
    const result = await getAllBooks();
    console.log(result);
    return {
      statusCode: 200,
      body: JSON.stringify(result)
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error.statusCode || 501,
      headers: { 'Content-Type': 'text/plain' },
      body: 'Couldn\'t get book record.',
    }
  }
};

const getAllHandler = middy(getAllResolver)
  .use(urlEncodeBodyParser({extended: true}))
  .use(jsonBodyParser())
  .use(httpErrorHandler());

module.exports = {
  getAll : getAllHandler
};
