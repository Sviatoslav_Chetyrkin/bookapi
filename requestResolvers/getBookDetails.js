const {get} = require('lodash');
const {getBooks} = require('../services/getBookDetails');

const getResolver = async (event) => {
  const bookId = get(event, 'pathParameters.bookId', '');

  const res = await getBooks(bookId);

  return {
    statusCode: 200,
    body: JSON.stringify(res)
  };
};


module.exports = {
  get : getResolver
};
