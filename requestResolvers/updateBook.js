const {updateBook} = require('../services/updateBook')

const updateResolver = async (event) => {
  const bookEntity = JSON.parse(event.body);

  try {
    const result = await updateBook(bookEntity);

    return {
      statusCode: 200,
      body: JSON.stringify(result)
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: error.statusCode || 501,
      headers: { 'Content-Type': 'text/plain' },
      body: 'Couldn\'t update the book record.',
    }
  }
};

module.exports = {
  updateResolver
};
