const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies
const {v4} = require('uuid');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

const addBook = async (bookEntity) => {
  const params = {
    TableName: process.env.BOOKS_TABLE,
    Item: {
      uuid: v4(),
      name: bookEntity.name,
      releaseDate: bookEntity.releaseDate,
      authorName: bookEntity.authorName
    },
    ReturnValues: "ALL_OLD"
  };

  await dynamoDb.put(params).promise();

  return params.Item;
};

module.exports = {
  addBook
};
