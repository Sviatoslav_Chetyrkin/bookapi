const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

const deleteBook = async (bookId) => {
  const params = {
    TableName: process.env.BOOKS_TABLE,
    Key: {
      uuid: bookId,
    },
  };

  await dynamoDb.delete(params).promise();
};

module.exports = {
  deleteBook
};
