const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

const getAllBooks = async () => {
  const params = {
    TableName: process.env.BOOKS_TABLE,
  };

  const res = await dynamoDb.scan(params).promise();

  return res.Items;
};

module.exports = {
  getAllBooks
};
