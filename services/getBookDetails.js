const AWS = require('aws-sdk');
const dynamoDb = new AWS.DynamoDB.DocumentClient();

const getBooks = async (bookId) => {
  const params = {
    TableName: process.env.BOOKS_TABLE,
    Key: {
      uuid: bookId,
    },
  };

  const res = await dynamoDb.get(params).promise();

  return res.Item;
};

module.exports = {
  getBooks
};
