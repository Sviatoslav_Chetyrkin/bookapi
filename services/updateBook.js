const AWS = require('aws-sdk'); // eslint-disable-line import/no-extraneous-dependencies
const dynamoDb = new AWS.DynamoDB.DocumentClient();

const updateBook = async (bookData) => {
  const params = {
    TableName: process.env.BOOKS_TABLE,
    Key: {
      uuid: bookData.uuid,
    },
    ExpressionAttributeNames: {
      '#releaseDate': 'releaseDate',
      '#authorName': 'authorName',
      '#name': 'name',
    },
    ExpressionAttributeValues: {
      ':name': bookData.name,
      ':releaseDate': bookData.releaseDate,
      ':authorName': bookData.authorName,
    },
    UpdateExpression: 'SET #name = :name, #releaseDate = :releaseDate, #authorName = :authorName',
    ReturnValues: 'ALL_NEW',
  };

  const result = await dynamoDb.update(params).promise();

  return result.Attributes;
};

module.exports = {
  updateBook
};
